const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseUniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    username: {type:String, required:true},
    email: {type:String, required:true, unique:true},
    password: {type:String, required:true},
    fullName: {type:String, required:true},
    userType: {type:Schema.Types.ObjectId, ref:'Usertype'},
    likes: [{type:Schema.Types.ObjectId, ref:'Like', default:[]}],
    comments: [{type:Schema.Types.ObjectId, ref:'Comment', default:[]}]
});

userSchema.plugin(mongooseUniqueValidator);

userSchema.pre("find", function(next){
    this.populate('userType');
    next();
});

userSchema.pre("findOne", function(next){
    this.populate('userType');
    next();
});

userSchema.pre("findById", function(next){
    this.populate('userType');
    next();
})

module.exports = mongoose.model('User',userSchema);

