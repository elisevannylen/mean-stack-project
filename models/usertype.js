const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userTypeSchema = new Schema({
    typeName: {type: String, required: true}
});

module.exports = mongoose.model('Usertype', userTypeSchema);