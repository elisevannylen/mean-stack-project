const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const posttextSchema = new Schema({
    text: {type: String, required: true}
});

module.exports = mongoose.model('Posttext',posttextSchema);