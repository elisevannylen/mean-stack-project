const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const idValidator = require('mongoose-id-validator');

const Like = require('./like');
const Comment = require('./comment');
const Picture = require('./picture');
const Posttext = require('./posttext');

const postSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    writeDate: {type: String, required: true},
    creationDate: {type: Schema.Types.Date, required: true},
    creator: {type: Schema.Types.ObjectId, ref:'User'},
    likes: [{type:Schema.Types.ObjectId, ref:'Like', default:[]}],
    comments: [{type:Schema.Types.ObjectId, ref:'Comment', default:[]}],
    tags: [{type:Schema.Types.ObjectId, ref:'Tag', default:[]}],
    pictures: [{type:Schema.Types.ObjectId, ref:'Picture', default:[]}],
    texts: [{type:Schema.Types.ObjectId, ref:'Posttext', default:[]}]
});

postSchema.plugin(idValidator);

postSchema.pre("remove", async function(next){
    await Like.remove({post:this._id});
    await Comment.remove({post:this._id});
    await Picture.remove({_id:{$in:this.pictures}});
    await Posttext.remove({_id:{$in:this.texts}});
});

module.exports = mongoose.model('Post',postSchema);