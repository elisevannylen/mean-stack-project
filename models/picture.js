var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pictureSchema = new Schema({
    description: {type: String, required: true},
    changeDate:{type:Schema.Types.Date, required: true}
});

pictureSchema.pre('validate', function(next){
	this.changeDate = new Date();
	next();
});

module.exports = mongoose.model('Picture',pictureSchema);