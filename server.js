const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const mongoose = require('mongoose');
const cors = require("cors");
const fileUpload = require('express-fileupload');

const postRoutes = require('./routes/posts');
const tagRoutes = require('./routes/tags');
const typeRoutes = require('./routes/usertypes');
const pictureRoutes = require('./routes/pictures');
const userRoutes = require('./routes/users');
const commentRoutes = require('./routes/comments');
const likeRoutes = require('./routes/likes');
const posttextRoutes = require('./routes/posttexts');
const authRoutes = require('./routes/auth');

const UserType = require("./models/usertype");
const User = require("./models/user");
const config = require('./config/auth');
const bcrypt = require('bcrypt');
const path = require("path");

async function init(){
    let countUserTypes = await UserType.count();

    if(countUserTypes == 0){
        let admin = new UserType();
        admin.typeName = "admin";
        await admin.save();

        let user = new UserType();
        user.typeName = "user";
        await user.save();
    }

    let countUsers = await User.count();

    if(countUsers == 0){
        let user = new User();
        user.username = "EliseVN";
        user.email = "elisevannylen@gmail.com";
        user.password = await bcrypt.hash("Elli1469", config.salt);
        user.fullName = "Elise Van Nylen";
        user.userType = await UserType.findOne({typeName:"admin"});
        user.likes = [];
        user.comments = [];
        await user.save();
    }
}

const app = express();
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/exchangeblog', {useMongoClient: true}).then(init);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

/*app.listen(3000, function () {
    console.log("server started");
});*/

app.use(cors());
app.use(fileUpload());
app.use("/posts", postRoutes);
app.use("/tags", tagRoutes);
app.use("/usertypes", typeRoutes);
app.use("/pictures", pictureRoutes);
app.use("/users", userRoutes);
app.use("/comments", commentRoutes);
app.use("/likes", likeRoutes);
app.use("/posttexts", posttextRoutes);
app.use("/auth", authRoutes);

// Download picture and save in files
app.get('/pictures/:pictureId/download', async function(req, res, next){
    try{
        res.sendFile(`${__dirname}/img/${req.params.pictureId}`);
    }catch(err){
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

app.use("/", express.static("public"));

app.use("/*", function(req, res){
	res.sendFile(path.join(__dirname, "public/index.html"))
});

const httpServer = http.createServer(app);
httpServer.listen(3000, function(){
    console.log("server is running");
});
