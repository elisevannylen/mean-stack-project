const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Posttext = require('../models/posttext');

//get all posttext
router.get('/', async function (req, res, next) {
    try{
        let posttexts = await Posttext.find();
        res.json(posttexts);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get one posttext
router.get('/:posttextId', async function (req, res, next) {
    try{
        let posttext = await Posttext.findById(req.params.posttextId);
        res.json(posttext);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//add posttext
router.post('/', auth.admin, async function (req, res, next) {
    let posttext = new Posttext(req.body);

    try{
        await posttext.save();
        res.status(201).json(posttext);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//update posttext
router.put("/:posttextId", auth.admin, async function(req,res){
	try{
		delete req.body._id;
		let posttext = await Posttext.findByIdAndUpdate(req.params.posttextId, req.body, null);
		res.json(posttext);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

//delete posttext
router.delete('/:posttextId', auth.admin, async function(req, res, next){
    try{
        let posttext = await Posttext.findByIdAndRemove(req.params.posttextId);
        res.json(posttext);
    }catch(err){
        console.log(err);
		res.status(500).json({err});
    }
});

module.exports = router;