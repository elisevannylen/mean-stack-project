const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Tag = require('../models/tag');

//get all tags
router.get('/', async function (req, res, next) {
    try{
        let tags = await Tag.find();
        res.json(tags);
    } catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get one tag
router.get('/:tagId', async function (req, res, next) {
    try{
        let tag = await Tag.findById(req.params.tagId);
        res.json(tag);
    } catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//add tag
router.post('/', auth.admin, async function (req, res, next) {
    let tag = new Tag(req.body);

    try{
        await tag.save();
        res.status(201).json(tag);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//update tag
router.put('/:tagId', auth.admin, async function (req, res, next) {
    try{
		delete req.body._id;
		let tag = await Tag.findByIdAndUpdate(req.params.tagId, req.body, null);
		res.json(tag);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
    }
});

//delete tag
router.delete('/:tagId', auth.admin, async function(req, res, next){
    try{
        let tag = await Tag.findByIdAndRemove(req.params.tagId);
        res.json(tag);
    }catch(err){
        console.log(err);
		res.status(500).json({err});
    }
});

module.exports = router;