const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Comment = require('../models/comment');

//get all comments
router.get('/', async function (req, res, next) {
    try{
        let comments = await Comment.find().populate("user").populate("post");
        res.json(comments);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get one comment
router.get('/:commentId', async function (req, res, next) {
    try{
        let comment = await Comment.findById(req.params.commentId).populate("user").populate("post");
        res.json(comment);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//add comment
router.post('/', auth.user, async function (req, res, next) {
    let comment = new Comment(req.body);

    try{
        await comment.save();
        await Comment.populate(comment, "user");
        await Comment.populate(comment, "post");
        res.status(201).json(comment);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//update comment
router.put("/:commentId", auth.user, async function(req,res){
	try{
		delete req.body._id;
        let comment = await Comment.findByIdAndUpdate(req.params.commentId, req.body, null);
        await Comment.populate(comment, "user");
        await Comment.populate(comment, "post");
		res.json(comment);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

//delete comment
router.delete('/:commentId', auth.user, async function(req, res, next){
    try{
        let comment = await Comment.findByIdAndRemove(req.params.commentId);
        res.json(comment);
    }catch(err){
        console.log(err);
		res.status(500).json({err});
    }
});

module.exports = router;