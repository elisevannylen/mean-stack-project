const express = require("express");
const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth");
const authM = require("../middleware/authentication");
const bcrypt = require('bcrypt');

const User = require("../models/user");

const router = express.Router();

router.post('/login', async function (req, res, next) {

    try{
        let user = await User.findOne({email: req.body.email});

        if (user == null){
            return res.status(401).json({
                title: 'Login failed',
                error: {message: 'Invalid login credentials'}
            });
        }

        if(!await bcrypt.compare(req.body.password, user.password)){
            return res.status(401).json({
                title: 'Login failed',
                error: {message: 'Invalid login credentials'}
            });
        }
        
        const token = jwt.sign({user: user}, authConfig.secret, {expiresIn:7200});
        res.json({
            message: 'Successfully logged in',
            token: token,
            user: user
        })

    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

router.get('/', authM.user, function (req, res, next) {
    res.json({});
});

//get one user
router.get('/find/:id', authM.user, async function (req, res, next) {
    try{
        let user = await User.findById(req.params.id);
        res.json(user);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;