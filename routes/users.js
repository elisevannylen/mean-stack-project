const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const auth = require('../middleware/authentication');
const User = require('../models/user');
const Usertype = require('../models/usertype');
const Config = require('../config/auth');

// Register user - only user
router.post('/register', async function (req, res, next) {
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, Config.salt),
        fullName: req.body.fullName,
        likes: [],
        comments: []
    });

    let type = await Usertype.findOne({typeName:"user"});
    user.userType = type._id;

    try{
        await user.save();
        res.json({message: "successfully saved"});
    }catch(err){
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// update user - only user
router.patch("/", auth.user, async function(req,res){
    let type = await Usertype.findOne({typeName:"user"});
    req.body.userType = type;
	try{
        delete req.body._id;
        if (req.body.password != null){
            req.body.password = await bcrypt.hash(req.body.password, Config.salt);
        } else {
            req.body.password = req.user.password;
        }
		let user = await User.findByIdAndUpdate(req.user._id, req.body, null);
		res.json(user);
	}catch(err){
		res.status(500).json({
            title: 'An error occurred',
            error: err
        });
	}
});

// Add user - users and admins
router.post('/', auth.admin, async function (req, res, next) {
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, Config.salt),
        fullName: req.body.fullName,
        userType: req.body.userType,
        likes: [],
        comments: []
    });

    try{
        await user.save();
        res.json({message: "successfully saved"});
    }catch(err){
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get all users
router.get('/', auth.admin, async function (req, res, next) {
    try{
        res.json(await User.find());
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get one user
router.get('/:id', auth.admin, async function (req, res, next) {
    try{
        let user = await User.findById(req.params.id);
        res.json(user);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// update user
router.put("/:userId", auth.admin, async function(req,res){
	try{
        delete req.body._id;
        if (req.body.password != null){
            req.body.password = await bcrypt.hash(req.body.password, Config.salt);
        } else {
            req.body.password = req.user.password;
        }
		let user = await User.findByIdAndUpdate(req.params.userId, req.body, null);
		res.json(user);
	}catch(err){
		res.status(500).json({
            title: 'An error occurred',
            error: err
        });
	}
});

// delete user
router.delete('/:userId', auth.admin, async function(req, res, next){
    try{
        let user = await User.findByIdAndRemove(req.params.userId);
        res.json(user);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;