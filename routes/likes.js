const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Like = require('../models/like');

//get all likes
router.get('/', async function (req, res, next) {
    try{
        let likes = await Like.find().populate("user").populate("post");
        res.json(likes);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//get one like
router.get('/:likeId', async function (req, res, next) {
    try{
        let like = await Like.findById(req.params.likeId).populate("user").populate("post");
        res.json(like);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//add like
router.post('/', auth.user, async function (req, res, next) {
    let like = new Like(req.body);

    try{
        await like.save();
        await Like.populate(like, "user");
        await Like.populate(like, "post");
        res.status(201).json(like);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

//delete like
router.delete('/:likeId', auth.user, async function(req, res, next){
    try{
        let like = await Like.findByIdAndRemove(req.params.likeId);
        res.json(like);
    }catch(err){
        console.log(err);
		res.status(500).json({err});
    }
});

module.exports = router;