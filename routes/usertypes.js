const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Usertype = require('../models/usertype');

// get all usertypes
router.get('/', async function (req, res, next) {
    try{
        let types = await Usertype.find();
        res.json(types);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// get one usertype
router.get('/:userTypeId', async function (req, res, next) {
    try{
        let type = await Usertype.findById(req.params.userTypeId);
        res.json(type);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// get one usertype by name
router.get('/byName/:userTypeName', async function (req, res, next) {
    try{
        let type = await Usertype.findOne({typeName:req.params.userTypeName});
        res.json(type);
    }catch(err){
        console.log(err);
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// add usertype
router.post('/', auth.admin, async function (req, res, next) {
    let type = new Usertype(req.body);

    try{
        await type.save();
        res.status(201).json(type);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// update usertype
router.put('/:userTypeId', auth.admin, async function (req, res, next) {
    try{
		delete req.body._id;
		let type = await Usertype.findByIdAndUpdate(req.params.userTypeId, req.body, null);
		res.json(type);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

// delete usertype
router.delete('/:userTypeId', auth.admin, async function(req, res, next){
    try{
        let type = await Usertype.findByIdAndRemove(req.params.userTypeId);
        res.json(type);
    }catch(err){
        console.log(err);
		res.status(500).json({err});
    }
});

module.exports = router;