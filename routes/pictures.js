const express = require('express');
const router = express.Router();
const fs = require('fs'); // filesystem

const auth = require('../middleware/authentication');
const Picture = require('../models/picture');

// Get all pictures
router.get('/', async function (req, res, next) {
    res.json(await Picture.find({}));
});

// Get one picture by id
router.get('/:pictureId', async function (req, res, next) {
    try{
        let picture = await Picture.findById(req.params.pictureId);
        res.json(picture);
    }catch(err){
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Add one picture
router.post('/', auth.admin, async function (req, res, next) {
    var picture = new Picture(req.body);

    try {
	console.log(req.body);
	console.log(req.files);
        if (!req.files || (req.files && !req.files.image)){
			throw new Error("No image provided");
		}
		let file = req.files.image;
		if (file.mimetype != "image/jpeg" && file.mimetype != "image/png"){
			throw new Error("File type of " + file.mimetype + " is not accepted");
		}
		await picture.save();
		try{
			await file.mv(`${__dirname}/../img/${picture._id}`);
		}catch(err){
			await picture.remove();
			throw err;
		}
		res.json(picture);

    } catch (err) {
        console.log(err);
		res.status(500).json(err);
    }
});

// Delete one picture
router.delete('/:pictureId', auth.admin, async function(req, res, next){
    try {
        let picture = await Picture.findById(req.params.pictureId);
        await picture.remove();
        res.json({});
        
        fs.unlink(`${__dirname}/../img/${picture._id}`);
    } catch (err) {
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;
