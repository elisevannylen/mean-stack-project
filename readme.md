﻿# MEAN stack Blog about Exchange year

The MEAN stack blog was a *school project* that survived after my education. It is one of this things that I wish to expand in my free time and add some features that would help making it better.

The blog exists of **3 enviroments**:
 1. The **visitor** that can read blogposts and comments
 2. The **logged-in user** that is able to like and comment on posts
 3. The **admin user** that can add, modify and delete posts, users & comments

## About MEAN stack

MEAN is a free and open-source JavaScript software stack for building dynamic web sites and web applications.

**MEAN** stands for **M**ongoDB, **E**xpress.js, **A**ngular, and **N**ode.js. MEAN applications can be written in one language both server-side and client-side because all the components mentioned above support prograls that are written in JavaScript.

## Bugs and Issues

Have a bug or an issue when visiting, reading or interacting with the blog? [Open a new issue](https://gitlab.com/elisevannylen/mean-stack-project/issues) here on GitLab. And I will try to fix the issues as soon as possible to make the blog better.
