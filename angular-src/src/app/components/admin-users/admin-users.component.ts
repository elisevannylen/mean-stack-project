import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
	selector: 'app-admin-users',
	templateUrl: './admin-users.component.html',
	styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
	users: User[];

	constructor(private router: Router, private userService: UserService) { }

	async ngOnInit() {
		this.getUsers();
	}

	async getUsers(){
		this.userService.getUsers().then(users=>this.users = users).then(function () {
			this.users = this.users.sort((a, b) => {
			  if (a.email < b.email) return -1;
			  else if (a.email > b.email) return 1;
			  else return 0;
			})
		}.bind(this));
	}

	async onDelete(id){
		if(confirm('Are you sure you want to delete this user?')){
			await this.userService.deleteUser(id);
		}
		this.getUsers();
	}
}
