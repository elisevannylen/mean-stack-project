import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-userinfo-edit',
	templateUrl: './userinfo-edit.component.html',
	styleUrls: ['./userinfo-edit.component.css']
})
export class UserinfoEditComponent implements OnInit {
	userId: string;
	user: User;
	userFound:boolean = false;

	constructor(private router: Router, private userService: UserService, private authService:AuthService) { }

	async ngOnInit() {
		this.user = new User();
		await this.getUser();
		this.user.password = null;
	}

	async getUser(){
		try {
			this.user = await this.authService.getCurrentUser();
			this.userFound = true;
		} catch (err) {
			console.log(err);
			this.userFound = false;
		}
	}

	async onSubmit(){
		let updateUser = new User(await this.userService.updateUserinfo(this.user));
		this.user = new User();
		this.router.navigate(['/']);
	}
}
