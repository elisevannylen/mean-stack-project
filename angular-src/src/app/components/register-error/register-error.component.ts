import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import {Router} from "@angular/router";

@Component({
	selector: 'app-register-error',
	templateUrl: './register-error.component.html',
	styleUrls: ['./register-error.component.css']
})
export class RegisterErrorComponent implements OnInit {

	constructor(private authService:AuthService, private router:Router) { }

	ngOnInit() {
	}

}
