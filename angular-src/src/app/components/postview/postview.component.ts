import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Post } from '../../models/post.model';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import { Like } from '../../models/like.model';
import { LikeService } from '../../services/like.service';
import { CommentService } from '../../services/comment.service';
import { Comment } from '../../models/comment.model';

@Component({
	selector: 'app-postview',
	templateUrl: './postview.component.html',
	styleUrls: ['./postview.component.css']
})
export class PostviewComponent implements OnInit, OnDestroy {
	private sub: any;
	postId:string;
	post:Post = new Post();
	likePost:boolean;
	currentUser:User = new User();
	likes:Like[] = [];
	comment:Comment = new Comment();
	comments:Comment[] = [];
	edit:boolean = false;
	commentIndex:number;
	loggedIn: boolean = false;

	constructor(private router: Router, private route: ActivatedRoute, private postService: PostService, 
		private authService:AuthService, private likeService:LikeService, private commentService:CommentService) { }

	async ngOnInit() {
		this.sub = await this.route.params.subscribe(params => {
			this.postId = params['id'];
			this.getPost();
			this.getUser();
			this.getLikes();
			this.getComments();

			this.comment.message = "";
		});
	}

	async getPost(){
		try {
			this.post = new Post(await this.postService.getPost(this.postId));
		} catch (error) {
			console.log(error);
		}
	}

	async getUser(){
		try {
			this.currentUser = await this.authService.getCurrentUser();
			this.loggedIn = true;
			this.userLikedPost();
		} catch (error) {
			this.loggedIn = false;
		}
	}

	async getLikes(){
		try {
			let allLikes = await this.likeService.getLikes();
			for (let i in allLikes){
				let like = new Like(allLikes[i]);
				if (like.post._id == this.post._id){
					this.likes.push(like);
				}
			}

			this.userLikedPost();
		} catch (error) {
			console.log(error);
		}
	}

	userLikedPost(){
		let index = this.likes.findIndex(l => l.user._id == this.currentUser._id);
		
		if (index == -1){
			this.likePost = false;
		} else {
			this.likePost = true;
		}
	}

	async getComments(){
		try {
			let allComments = await this.commentService.getComments();
			for (let i in allComments){
				let comment = new Comment(allComments[i]);
				if (comment.post._id == this.post._id){
					this.comments.push(comment);
				}
			}
		} catch (error) {
			console.log(error);
		}
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	async onLike(){
		try {
			if (this.currentUser != null){
				let like = new Like();
				like.post = this.post;
				like.user = this.currentUser;
	
				let result = new Like(await this.likeService.likePost(like));
				this.likes.push(result);
				this.likePost = true;
			}
		} catch (error) {
			console.log(error);
		}
	}

	async onUnlike(){
		try {
			if (this.currentUser != null){
				let index = this.likes.findIndex(l => l.user._id == this.currentUser._id);
				if (index != -1){
					let likeId = this.likes[index]._id;
					await this.likeService.unlikePost(likeId);
					this.likes.splice(index, 1);
					this.likePost = false;
				}
			}
		} catch (error) {
			console.log(error);
		}
	}

	async onSubmit(){
		try {
			if (this.comment.message != ""){
				if (this.currentUser != null){
					if(!this.edit){
						// create new comment
						this.comment.post = this.post;
						this.comment.user = this.currentUser;
			
						let result = new Comment(await this.commentService.createComment(this.comment));
						this.comments.push(result);
					} else {
						// update comment
						let result = await this.commentService.updateComment(this.comment);
						await this.comments.splice(this.commentIndex, 1, this.comment);
					}
				}

			}
			this.comment = new Comment();
			this.edit = false;
		} catch (error) {
			console.log(error);
		}
	}

	async onDelete(commnt:Comment){
		try {
			if (this.currentUser != null){
				let index = this.comments.findIndex(c => c._id == commnt._id);
				if (index != -1){
					await this.commentService.deleteComment(commnt._id);
					this.comments.splice(index, 1);
				}
			}
		} catch (error) {
			console.log(error);
		}
	}

	async onEdit(commnt:Comment){
		if (this.currentUser != null){
			this.comment = commnt;
			this.edit = true;
			this.commentIndex = this.comments.findIndex(c => c._id === commnt._id);
		}
	}
}
