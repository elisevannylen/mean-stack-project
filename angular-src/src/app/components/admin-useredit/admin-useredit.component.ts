import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { Usertype } from '../../models/usertype.model';

@Component({
	selector: 'app-admin-useredit',
	templateUrl: './admin-useredit.component.html',
	styleUrls: ['./admin-useredit.component.css']
})
export class AdminUsereditComponent implements OnInit, OnDestroy {
	userId: string;
	user: User;
	usertypes: Usertype[];
	usertype: Usertype;
	usertypeId: string;
	private sub: any;

	constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) { }

	async ngOnInit() {
		this.user = new User();
		this.sub = this.route.params.subscribe(params => {this.userId = params['id'];});
		this.user = await this.userService.getUser(this.userId);
		this.user.password = null;
		let usertype = new Usertype(this.user.userType);
		this.usertypeId = usertype._id;

		this.usertypes = await this.userService.getUsertypes();
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	async onSubmit(){
		this.user.userType = await this.userService.getUserType(this.usertypeId);
		let updateUser = new User(await this.userService.updateUser(this.user));
		this.user = new User();
		this.router.navigate(['/users']);
	}
}
