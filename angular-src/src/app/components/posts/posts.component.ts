import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post.model';
import { Router } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Tag } from '../../models/tag.model';
import { TagService } from '../../services/tag.service';

@Component({
	selector: 'app-posts',
	templateUrl: './posts.component.html',
	styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
	posts:Post[];
	tags:Tag[];

	constructor(private router:Router, private postService:PostService, private tagService:TagService) { }

	async ngOnInit() {
		await this.getPosts();
		await this.getTags();
	}

	async getPosts(){
		try {
			this.posts = await this.postService.getPosts();
		} catch (error) {
			console.log(error);
		}
	}

	async getTags(){
		try {
			this.tags = await this.tagService.getTags();
		} catch (error) {
			console.log(error);
		}
	}

}
