import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-admin-panel',
	templateUrl: './admin-panel.component.html',
	styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
	user: User = new User();

	constructor(private router: Router, private authService:AuthService) { }

	async ngOnInit() {
		try {
			this.user = await this.authService.getCurrentUser();
		} catch (err) {
			console.log(err);
		}
	}

}
