import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { Tag } from '../../models/tag.model';
import { TagService } from '../../services/tag.service';

@Component({
	selector: 'app-admin-post-tags',
	templateUrl: './admin-post-tags.component.html',
	styleUrls: ['./admin-post-tags.component.css']
})
export class AdminPostTagsComponent implements OnInit, OnDestroy {
	post:Post;
	postId:string;
	private sub: any;
	allTags:Tag[];
	tagId:string;
	isEmpty:boolean = false;
	
	constructor(private router:Router, private route: ActivatedRoute, private postService:PostService, private tagService:TagService) { }

	async ngOnInit() {
		this.post = new Post();
		this.sub = this.route.params.subscribe(params => {this.postId = params['id'];});
		await this.getPost();
		await this.makeTagArray();
	}

	async getPost(){
		try {
			this.post = await this.postService.getPost(this.postId);
		} catch (err) {
			console.log(err);
		}
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	async onDeleteAll(){
		this.post.tags = [];
		this.post = new Post(await this.postService.updatePost(this.post));
		await this.getPost();
		await this.makeTagArray();
	}

	async onSubmit(){
		if(this.tagId != null){
			let tagObj = new Tag(await this.tagService.getTag(this.tagId));
			this.post.tags.push(tagObj);
			this.post = new Post(await this.postService.updatePost(this.post));
			await this.getPost();
			await this.makeTagArray();
		}
	}

	async makeTagArray(){
		let tags = new Array<Tag>();
		this.allTags = [];

		try{
			tags = await this.tagService.getTags();
			
			for (let tg of tags)
			{
				let index = this.post.tags.findIndex(t => t._id === tg._id);
				if (index == -1){
					this.allTags.push(tg);
				}
			}
			
			if (this.post.tags.length == 0){
				this.isEmpty = true;
			} else {
				this.isEmpty = false;
			}
		}catch(err){
			console.log(err);
		}
	}

	async onDelete(tag:Tag){
		if(confirm('Are you sure you want to delete this tag?')){
			try {
				let index = this.post.tags.findIndex(t => t._id === tag._id);
				this.post.tags.splice(index, 1);

				this.post = new Post(await this.postService.updatePost(this.post));
				await this.getPost();
				await this.makeTagArray();
			} catch (err) {
				console.log(err);
			}
		}
	}
}
