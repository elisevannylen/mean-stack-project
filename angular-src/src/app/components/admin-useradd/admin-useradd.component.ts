import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { Usertype } from '../../models/usertype.model';

@Component({
	selector: 'app-admin-useradd',
	templateUrl: './admin-useradd.component.html',
	styleUrls: ['./admin-useradd.component.css']
})
export class AdminUseraddComponent implements OnInit {
	user: User = new User();
	usertypes: Usertype[] = [];
	usertypeId: string;

	constructor(private router: Router, private userService: UserService) { }

	async ngOnInit() {
		this.usertypes = await this.userService.getUsertypes();
		let usertype = new Usertype(this.user.userType);
		this.usertypeId = usertype._id;
	}

	async onSubmit() {
		this.user.userType = await this.userService.getUserType(this.usertypeId);
		await this.userService.createUser(this.user);
		this.user = new User();
		this.router.navigate(['/users']);
	}
}
