import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post.model';
import { CommentService } from '../../services/comment.service';
import { PostService } from '../../services/post.service';
import { Router } from '@angular/router';
import { Comment } from '../../models/comment.model';

@Component({
	selector: 'app-admin-comments',
	templateUrl: './admin-comments.component.html',
	styleUrls: ['./admin-comments.component.css']
})
export class AdminCommentsComponent implements OnInit {
	posts:Post[] = [];
	fullPosts:Post[] = [];

	constructor(private router: Router, private postService:PostService, private commentService:CommentService) { }

	async ngOnInit() {
		await this.getPosts();
		await this.getComments();
	}

	async getPosts(){
		try {
			this.posts = await this.postService.getPosts();
		} catch (err) {
			console.log(err);
		}
	}

	async getComments(){
		try {
			this.fullPosts = [];
			let allComments = await this.commentService.getComments();
			for (let i in this.posts){
				let post = new Post(this.posts[i]);
				post.comments = [];

				for (let i in allComments){
					let comment = new Comment(allComments[i]);
					if (comment.post._id == post._id){
						post.comments.push(comment);
					}
				}

				this.fullPosts.push(post);
			}
		} catch (err) {
			console.log(err);
		}
	}

	async onDelete(comment:Comment){
		if(confirm('Are you sure you want to delete this comment?')){
			try {
				await this.commentService.deleteComment(comment._id);
				await this.getComments();
			} catch (err) {
				console.log(err);
			}
		}
		this.getPosts();
	}

}
