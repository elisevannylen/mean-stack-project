import { Component, OnInit } from '@angular/core';
import { Tag } from '../../models/tag.model';
import { Router } from '@angular/router';
import { TagService } from '../../services/tag.service';

@Component({
	selector: 'app-admin-tag',
	templateUrl: './admin-tag.component.html',
	styleUrls: ['./admin-tag.component.css']
})
export class AdminTagComponent implements OnInit {
	tag:Tag = new Tag();
	tags:Tag[] = [];

	constructor(private router:Router, private tagService:TagService) { }

	async ngOnInit() {
		await this.getTags();
	}

	async getTags(){
		try {
			this.tags = await this.tagService.getTags();
		} catch (err) {
			console.log(err);
		}
	}

	async onSubmit(){
		try {
			let index = this.tags.findIndex(t => t.tagName == this.tag.tagName);
			if (index == -1){
				await this.tagService.createTag(this.tag);
			}
			this.tag = new Tag();
			await this.getTags();
		} catch (err) {
			console.log(err);
		}
	}
}
