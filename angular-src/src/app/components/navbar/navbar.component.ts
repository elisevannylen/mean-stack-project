import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {User} from "../../models/user.model";

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
	isAdmin: boolean;
	isLoggedin: boolean;
	
	constructor(private router: Router, private service: AuthService) {
		this.isLoggedin = service.isLoggedIn();
		this.isAdmin = service.isAdmin();
		
		//eventemmitters in Auth.service send warning when logged in or logged out
		service.onLogin.subscribe(loggedIn => this.isLoggedin = service.isLoggedIn());
		service.onLogout.subscribe(loggedIn => this.isLoggedin = service.isLoggedIn());
		service.onLogin.subscribe(loggedIn => this.isAdmin = service.isAdmin());
		service.onLogout.subscribe(loggedIn => this.isAdmin = service.isAdmin());
	}
	
	ngOnInit() {
	}

	onLogout(){
		this.service.logout();
		this.router.navigate(['/']);
	}
}