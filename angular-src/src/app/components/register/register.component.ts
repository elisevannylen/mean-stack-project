import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { Router } from "@angular/router";
import { User } from "../../models/user.model";

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	user:User = new User();

	constructor(private userService:UserService, private router: Router) { }

	ngOnInit() {
	}

	async onSubmit() {
		await this.userService.register(this.user);
		this.user = new User();
  	}

}
