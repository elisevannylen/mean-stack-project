import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { PictureService } from '../../services/picture.service';
import { Picture } from '../../models/picture.model';

@Component({
	selector: 'app-admin-post-pictures',
	templateUrl: './admin-post-pictures.component.html',
	styleUrls: ['./admin-post-pictures.component.css']
})
export class AdminPostPicturesComponent implements OnInit, OnDestroy {
	post:Post = new Post();
	postId:string;
	private sub: any;
	pictureFile:any;
	picture:Picture = new Picture();
	pictures:Picture[] = [];
	
	constructor(private router:Router, private route: ActivatedRoute, private postService:PostService, private pictureService:PictureService) { }

	async ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.postId = params['id'];
			this.getPost();
		});
	}

	async getPost(){
		try {
			this.post = await this.postService.getPost(this.postId);
			this.pictures = this.post.pictures;
		} catch (err) {
			console.log(err);
		}
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	fileChange(event){
		let files = event.target.files;
		this.pictureFile = files?files[0]:null;
	}

	async onSubmit(){
		try {
			this.picture = await this.pictureService.uploadPicture(this.picture, this.pictureFile);
			this.post.pictures.push(this.picture);
			this.post = await this.postService.updatePost(this.post);
			await this.getPost();
			this.picture = new Picture();
			this.pictureFile = null;
		} catch (err) {
			console.log(err);
		}
	}

	async onDelete(id){
		if(confirm('Are you sure you want to delete this picture?')){
			try {
				await this.pictureService.deletePicture(id);
				await this.getPost();
			} catch (err) {
				console.log(err);
			}
		}
	}

	async onDeleteAll(){
		if(confirm('Are you sure you want to delete all the pictures?')){
			try {
				for (let img of this.post.pictures){
					await this.pictureService.deletePicture(img._id);
				}
				this.post.pictures = [];
				this.post = await this.postService.updatePost(this.post);
				await this.getPost();
			} catch (err) {
				console.log(err);
			}
		}
	}
}
