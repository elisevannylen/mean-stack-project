import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user.model';

@Component({
	selector: 'app-admin-postadd',
	templateUrl: './admin-postadd.component.html',
	styleUrls: ['./admin-postadd.component.css']
})
export class AdminPostaddComponent implements OnInit {
	post:Post = new Post();
	user:User;

	constructor(private router:Router, private postService:PostService, private authS:AuthService) { }

	async ngOnInit() {
		try {
			this.user = await this.authS.getCurrentUser();

			this.post.creator = this.user;
		} catch (err) {
			console.log(err);
		}
	}

	async onSubmit(){
		try {
			await this.postService.createPost(this.post);
			this.post = new Post();
			this.router.navigate(['/postsadmin']);
		} catch (err) {
			console.log(err);
		}
	}

}
