import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { Posttext } from '../../models/posttext.model';
import { PosttextService } from '../../services/posttext.service';

@Component({
	selector: 'app-admin-post-text',
	templateUrl: './admin-post-text.component.html',
	styleUrls: ['./admin-post-text.component.css']
})
export class AdminPostTextComponent implements OnInit, OnDestroy {
	post:Post;
	postId:string;
	private sub: any;
	posttext:Posttext = new Posttext();
	texts:Posttext[];
	edit:boolean = false;
	textIndex:number;
	
	constructor(private router:Router, private route: ActivatedRoute, private postService:PostService, private textService:PosttextService) { }

	async ngOnInit() {
		this.post = new Post();
		this.sub = this.route.params.subscribe(params => {
			this.postId = params['id'];
			this.getPost();
		});
	}

	async getPost(){
		try {
			this.post = await this.postService.getPost(this.postId);
			this.texts = this.post.texts;
		} catch (err) {
			console.log(err);
		}
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	async onSubmit(){
		try {
			if (!this.edit){
				// create new text
				let text = new Posttext(await this.textService.createText(this.posttext));
				this.post.texts.push(text);
			} else {
				// update text
				let text = new Posttext(await this.textService.updateText(this.posttext));
				this.post.texts.splice(this.textIndex, 1, text);
			}
			this.post = new Post(await this.postService.updatePost(this.post));
			this.getPost();

			this.posttext = new Posttext();
			this.posttext.text = "";
			this.edit = false;
		} catch (err) {
			console.log(err);
		}
	}

	async onEdit(text:Posttext){
		this.posttext = text;
		this.edit = true;
		this.textIndex = this.post.texts.findIndex(t => t._id === text._id);
	}

	async onDelete(text:Posttext){
		if(confirm('Are you sure you want to delete this text?')){
			try {
				await this.textService.deleteText(text._id);
				let index = this.post.texts.findIndex(t => t._id === text._id);
				this.post.texts.splice(index, 1);
				this.texts.splice(index, 1);
				
				this.post = new Post(await this.postService.updatePost(this.post));
				this.getPost();
			} catch (err) {
				console.log(err);
			}
		}
	}

}
