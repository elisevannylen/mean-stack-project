import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TagService } from '../../services/tag.service';
import { PostService } from '../../services/post.service';
import { Tag } from '../../models/tag.model';
import { Post } from '../../models/post.model';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
	selector: 'app-posts-tag',
	templateUrl: './posts-tag.component.html',
	styleUrls: ['./posts-tag.component.css']
})
export class PostsTagComponent implements OnInit, OnDestroy {
	private sub: any;
	tagId:string;
	tag:Tag = new Tag();
	posts:Post[] = [];

	constructor(private router: Router, private route: ActivatedRoute, private tagService:TagService, private postService:PostService) { }

	async ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.tagId = params['id'];
			this.getTag();
			this.getPosts();
		});
	}

	async getTag(){
		try {
			this.tag = new Tag(await this.tagService.getTag(this.tagId));
		} catch (error) {
			console.log(error);
		}
	}

	async getPosts(){
		try {
			this.posts = await this.postService.getPostsByTag(this.tagId);
		} catch (error) {
			console.log(error);
		}
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

}
