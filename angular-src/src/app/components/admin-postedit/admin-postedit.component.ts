import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';

@Component({
	selector: 'app-admin-postedit',
	templateUrl: './admin-postedit.component.html',
	styleUrls: ['./admin-postedit.component.css']
})
export class AdminPosteditComponent implements OnInit, OnDestroy {
	post:Post;
	postId:string;
	private sub: any;

	constructor(private router:Router, private route: ActivatedRoute, private postService:PostService) { }

	async ngOnInit() {
		this.post = new Post();
		this.sub = this.route.params.subscribe(params => {this.postId = params['id'];});
		this.getPost();
	}

	async getPost(){
		try {
			this.post = await this.postService.getPost(this.postId);
		} catch (err) {
			console.log(err);
		}
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	async onSubmit(){
		try {
			await this.postService.updatePost(this.post);
			this.post = new Post();
			this.router.navigate(['/postsadmin']);
		} catch (err) {
			console.log(err);
		}
	}
}
