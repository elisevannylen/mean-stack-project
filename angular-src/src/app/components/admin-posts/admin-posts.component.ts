import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { User } from '../../models/user.model';


@Component({
	selector: 'app-admin-posts',
	templateUrl: './admin-posts.component.html',
	styleUrls: ['./admin-posts.component.css']
})
export class AdminPostsComponent implements OnInit {
	posts:Post[];

	constructor(private router: Router, private postService:PostService) { }

	async ngOnInit() {
		this.getPosts();
	}

	async getPosts(){
		try {
			this.posts = await this.postService.getPosts();
		} catch (err) {
			console.log(err);
		}
	}

	async onDelete(id){
		if(confirm('Are you sure you want to delete this user?')){
			try {
				await this.postService.deletePost(id);
			} catch (err) {
				console.log(err);
			}
		}
		this.getPosts();
	}

}
