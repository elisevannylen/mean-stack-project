import { Routes } from '@angular/router';

import { LoginComponent } from "./components/login/login.component";
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from "./components/register/register.component";
import { PostsComponent } from "./components/posts/posts.component";
import { UserinfoEditComponent } from "./components/userinfo-edit/userinfo-edit.component";

import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AdminUsersComponent } from "./components/admin-users/admin-users.component";
import { AdminUseraddComponent } from "./components/admin-useradd/admin-useradd.component";
import { AdminUsereditComponent } from "./components/admin-useredit/admin-useredit.component";
import { AdminPostsComponent } from './components/admin-posts/admin-posts.component';
import { AdminPostaddComponent } from './components/admin-postadd/admin-postadd.component';
import { AdminPosteditComponent } from './components/admin-postedit/admin-postedit.component';
import { AdminPostTextComponent } from './components/admin-post-text/admin-post-text.component';
import { AdminPostPicturesComponent } from './components/admin-post-pictures/admin-post-pictures.component';
import { AdminPostTagsComponent } from './components/admin-post-tags/admin-post-tags.component';
import { AdminTagComponent } from './components/admin-tag/admin-tag.component';
import { PostsTagComponent } from './components/posts-tag/posts-tag.component';
import { PostviewComponent } from './components/postview/postview.component';
import { AdminCommentsComponent } from './components/admin-comments/admin-comments.component';

export const routes: Routes = [
	{path: "", component: PostsComponent},
	{path: "login", component: LoginComponent},
	{path: "logout", component: LogoutComponent},
	{path: "register", component: RegisterComponent},
	{path: "editUserinfo", component: UserinfoEditComponent},
	{path: "tag/:id", component: PostsTagComponent},
	{path: "post/:id", component: PostviewComponent},
	
	{path: "panel", component: AdminPanelComponent},
	{path: "users", component: AdminUsersComponent},
	{path: "addUser", component: AdminUseraddComponent},
	{path: "editUser/:id", component: AdminUsereditComponent},
	{path: "postsadmin", component: AdminPostsComponent},
	{path: "addPost", component:AdminPostaddComponent},
	{path: "editPost/:id", component: AdminPosteditComponent},
	{path: "addText/:id", component: AdminPostTextComponent},
	{path: "addPictures/:id", component: AdminPostPicturesComponent},
	{path: "addTags/:id", component: AdminPostTagsComponent},
	{path: "tagsadmin", component:AdminTagComponent},
	{path: "comments", component:AdminCommentsComponent}
];

