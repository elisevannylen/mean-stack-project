import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";

import { routes } from './app.routes';

import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { PostService } from './services/post.service';
import { CommentService } from './services/comment.service';
import { LikeService } from './services/like.service';
import { TagService } from './services/tag.service';
import { PictureService } from './services/picture.service';
import { PosttextService } from './services/posttext.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { PostsComponent } from './components/posts/posts.component';
import { RegisterComponent } from './components/register/register.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { AdminUseraddComponent } from './components/admin-useradd/admin-useradd.component';
import { AdminUsereditComponent } from './components/admin-useredit/admin-useredit.component';
import { UserinfoEditComponent } from './components/userinfo-edit/userinfo-edit.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AdminPostsComponent } from './components/admin-posts/admin-posts.component';
import { AdminPostaddComponent } from './components/admin-postadd/admin-postadd.component';
import { AdminPosteditComponent } from './components/admin-postedit/admin-postedit.component';
import { AdminPostTextComponent } from './components/admin-post-text/admin-post-text.component';
import { AdminPostPicturesComponent } from './components/admin-post-pictures/admin-post-pictures.component';
import { AdminPostTagsComponent } from './components/admin-post-tags/admin-post-tags.component';
import { AdminTagComponent } from './components/admin-tag/admin-tag.component';
import { PostsTagComponent } from './components/posts-tag/posts-tag.component';
import { PostviewComponent } from './components/postview/postview.component';
import { AdminCommentsComponent } from './components/admin-comments/admin-comments.component';
import { RegisterErrorComponent } from './components/register-error/register-error.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    PostsComponent,
    RegisterComponent,
    AdminUsersComponent,
    AdminUseraddComponent,
    AdminUsereditComponent,
    UserinfoEditComponent,
    HeaderComponent,
    FooterComponent,
    LogoutComponent,
    AdminPanelComponent,
    AdminPostsComponent,
    AdminPostaddComponent,
    AdminPosteditComponent,
    AdminPostTextComponent,
    AdminPostPicturesComponent,
    AdminPostTagsComponent,
    AdminTagComponent,
    PostsTagComponent,
    PostviewComponent,
    AdminCommentsComponent,
    RegisterErrorComponent
  ],
  imports: [
    BrowserModule,
    AlertModule.forRoot(),
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    UserService,
    PostService,
    CommentService,
    LikeService,
    TagService,
    PictureService,
    PosttextService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
