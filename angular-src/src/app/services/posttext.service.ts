import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import autoBind from 'auto-bind';
import { Posttext } from '../models/posttext.model';

@Injectable()
export class PosttextService {

	constructor(private http: HttpClient) { }

	async getTexts():Promise<Array<Posttext>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/posttexts", {headers}).toPromise();

		let texts:Array<Posttext> = [];
		for (let i in results){
			let text = new Posttext(results[i]);
			texts.push(text);
		}
		return texts;
	}

	async getText(textId:string):Promise<Posttext>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let result = await this.http.get(Config.host + "/posttexts/" + textId, {headers}).toPromise();
		return new Posttext(result);
	}

	async createText(text:Posttext):Promise<Posttext>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/posttexts", text, {headers}).toPromise();
		return new Posttext(result);
	}

	async updateText(text:Posttext):Promise<Posttext>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.put(Config.host + "/posttexts/" + text._id, text, {headers}).toPromise();
		return new Posttext(result);
	}

	async deleteText(textId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/posttexts/" + textId, {headers}).toPromise();
	}
}
