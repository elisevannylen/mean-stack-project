import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import autoBind from 'auto-bind';
import { Tag } from '../models/tag.model';

@Injectable()
export class TagService {

	constructor(private http: HttpClient) { }

	async getTags():Promise<Array<Tag>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/tags", {headers}).toPromise();

		let tags:Array<Tag> = [];
		for (let i in results){
			let tag = new Tag(results[i]);
			tags.push(tag);
		}
		let sortedTags:Array<Tag> = tags.sort((a,b) => {
			if (a.tagName > b.tagName) {
				return 1;
			}
		
			if (a.tagName < b.tagName) {
				return -1;
			}
		
			return 0;
		});
		return sortedTags;
	}

	async getTag(tagId:string):Promise<Tag>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let result = await this.http.get(Config.host + "/tags/" + tagId, {headers}).toPromise();
		return new Tag(result);
	}

	async createTag(tag:Tag):Promise<Tag>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/tags", tag, {headers}).toPromise();
		return new Tag(result);
	}

	async updateTag(tag:Tag):Promise<Tag>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.put(Config.host + "/tags/" + tag._id, tag, {headers}).toPromise();
		return new Tag(result);
	}

	async deleteTag(tagId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/tags/" + tagId, {headers}).toPromise();
	}
}
