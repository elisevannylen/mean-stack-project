import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import autoBind from 'auto-bind';
import { Post } from '../models/post.model';
import { Picture } from '../models/picture.model';
import { PostService } from '../services/post.service';

@Injectable()
export class PictureService {

	constructor(private http: HttpClient) { }

	async uploadPicture(picture:Picture, file:any):Promise<Picture>{
		let formData = new FormData();
		formData.append("image", file);
		formData.append("description", picture.description);

		let headers = new HttpHeaders().set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/pictures", formData, {headers}).toPromise();
		let savedPicture = new Picture(result);
		return savedPicture;
	}

	async deletePicture(pictureId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/pictures/" + pictureId, {headers}).toPromise();
	}
}
