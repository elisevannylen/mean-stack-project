import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import autoBind from 'auto-bind';
import { Like } from '../models/like.model';

@Injectable()
export class LikeService {

	constructor(private http: HttpClient) { }

	async getLikes():Promise<Array<Like>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/likes", {headers}).toPromise();

		let likes:Array<Like> = [];
		for (let i in results){
			let like = new Like(results[i]);
			likes.push(like);
		}
		return likes;
	}

	async getLike(likeId:string):Promise<Like>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let result = await this.http.get(Config.host + "/likes/" + likeId, {headers}).toPromise();
		return new Like(result);
	}

	async likePost(like:Like):Promise<Like>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/likes", like, {headers}).toPromise();
		return new Like(result);
	}

	async unlikePost(likeId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/likes/" + likeId, {headers}).toPromise();
	}

}
