import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import { Comment } from '../models/comment.model';

@Injectable()
export class CommentService {

	constructor(private http: HttpClient) { }

	async getComments():Promise<Array<Comment>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/comments", {headers}).toPromise();

		let comments:Array<Comment> = [];
		for (let i in results){
			let comment = new Comment(results[i]);
			comments.push(comment);
		}
		return comments;
	}

	async getComment(commentId:string):Promise<Comment>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let result = await this.http.get(Config.host + "/comments/" + commentId, {headers}).toPromise();
		return new Comment(result);
	}

	async createComment(comment:Comment):Promise<Comment>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/comments", comment, {headers}).toPromise();
		return new Comment(result);
	}

	async updateComment(comment:Comment):Promise<Comment>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.put(Config.host + "/comments/" + comment._id, comment, {headers}).toPromise();
		return new Comment(result);
	}

	async deleteComment(commentId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/comments/" + commentId, {headers}).toPromise();
	}

}
