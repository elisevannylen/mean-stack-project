import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { Usertype } from '../models/usertype.model';
import autoBind from 'auto-bind';
import Config from "../config";

@Injectable()
export class UserService {
	users: User[];
	user: User;
	userId: string;

	userType:Usertype;
	userTypes:Usertype[];

	constructor(private http: HttpClient) { }

	// create/register a new user
	async register(user:User){
		let userType = await this.getUserTypeByName("user");
		user.userType = userType;

		try{
			let headers = new HttpHeaders().set('content-type', 'application/json');
		  	return await this.http.post(Config.host + "/users/register/", user, {headers}).toPromise();
		}catch(err){
		  	console.log(err);
		}
	}

	// update user information
	async updateUserinfo(user:User){
		let userTypeUser = await this.getUserTypeByName("user");
		user.userType = userTypeUser

		try{
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			return await this.http.patch(Config.host + "/users", user, {headers}).toPromise();
		}catch(err){
		  	console.log(err);
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// METHODS FOR ADMIN PAGE
	//-----------------------------------------------------------------------------------------------------------------------------------

	// get all users
	async getUsers(){
		try{
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			let result: any = await this.http.get(Config.host + "/users", {headers}).toPromise();
			this.users = result;
			return this.users;
		}catch(err){
			console.log(err);
		}
	}

	// create new user
	async createUser(user:User){
		try{
		  	let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		  	return await this.http.post(Config.host + "/users", user, {headers}).toPromise();
		}catch(err){
		  	console.log(err);
		}
	}

	// get user
	async getUser(userId:string){
		try {
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			let result: any = await this.http.get(Config.host + "/users/" + userId, {headers}).toPromise();
			this.user = new User(result);
			return this.user;
		} catch (err) {
			console.log(err);
		}
	}

	// update user
	async updateUser(user:User){
		try{
		  	let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		  	return await this.http.put(Config.host + "/users/" + user._id, user, {headers}).toPromise();
		}catch(err){
		  	console.log(err);
		}
	}
	
	// delete user
	async deleteUser(userId:string){
		try{
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			return await this.http.delete(Config.host + `/users/` + userId, {headers:headers}).toPromise();
		}catch(err){
			console.log(err);
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// USERTYPE
	//-----------------------------------------------------------------------------------------------------------------------------------

	// get all usertypes
	async getUsertypes(){
		try {
			let result: any = await this.http.get(Config.host + "/usertypes").toPromise();
      		this.userTypes = result;
      		return this.userTypes;
		} catch (err) {
			console.log(err);
		}
	}

	// get usertype by Id
	async getUserType(typeId:string){
		try{
			let result: any = await this.http.get(Config.host + "/usertypes/" + typeId).toPromise();
			this.userType = new Usertype (result);
			return this.userType;
		}catch(err){
			console.log(err);
		}
	}

	// get usertype by Id
	async getUserTypeString(typeId:string){
		try{
			let result: any = await this.http.get(Config.host + "/usertypes/" + typeId).toPromise();
			return result;
		}catch(err){
			console.log(err);
		}
	}

	// get userType by name
	async getUserTypeByName(typeName:string){
		try{
			let result: any = await this.http.get(Config.host + "/usertypes/byName/" + typeName).toPromise();
			let userType = new Usertype (result);
			return userType;
		}catch(err){
			console.log(err);
		}
	}
}
