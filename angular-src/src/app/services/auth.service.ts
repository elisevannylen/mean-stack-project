import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { Usertype } from '../models/usertype.model';
import { UserService } from './user.service';
import autoBind from 'auto-bind';
import Config from "../config";

@Injectable()
export class AuthService {
	onLogout: EventEmitter<User> = new EventEmitter();
	onLogin: EventEmitter<User> = new EventEmitter();
	onUserFetched: EventEmitter<User> = new EventEmitter();

	user:User;
	userId: string;
	typeAdminId: string;

	constructor(private http: HttpClient, private userService: UserService) {
		//autoBind(this);
		if (localStorage.getItem("authKey") == null){
			localStorage.setItem("authKey", "");
		}
	}

	isLoggedIn(){
		if (localStorage.getItem("authKey")){
		  return true;
		}
		return false;
	}
	
	isAdmin(){
		if (localStorage.getItem("isAdmin")){
		  return this.isLoggedIn();
		}
		return false;
	}

	logout(){
		localStorage.clear();
		localStorage.setItem("authKey", "");
		this.onLogout.emit();
	}
	
	async getCurrentUser():Promise<User>{
		this.userId = localStorage.getItem("userId");
		if (this.userId != null){
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			let result = await this.http.get(Config.host + "/auth/find/" + this.userId, {headers}).toPromise();
			this.user = new User(result);
			return this.user;
		}
	}
	
	async login(email: string, password: string){
		let adminType = await this.userService.getUserTypeByName("admin");
		try{
			let headers = new HttpHeaders().set('content-type', 'application/json');
			let result: any = await this.http.post(Config.host + "/auth/login", {email, password},{headers:headers}).toPromise();
			this.user = new User(result.user);
			localStorage.setItem("authKey", result.token);
			localStorage.setItem("userId", result.user._id);
			let thisUsetype = new Usertype(this.user.userType);
			if( thisUsetype._id == adminType._id){
				localStorage.setItem("isAdmin", "yes");
			}
			this.onLogin.emit(this.user);
			return true;
		}catch(err){
		  	console.log(err);
		  	return false;
		}
	}

	async sendMessage(email: string, name: string, message: string){
		//TODO give this methode a email function
		try {
			return true
		} catch (err) {
			console.log(err);
			return false;
		}
	}

	async validateToken(){
		if (localStorage.getItem("authKey") != null && localStorage.getItem("authKey") != ""){
			let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
			let result: any = await this.http.get(Config.host + "/auth",{headers:headers}).toPromise();
		}
	}

}
