import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Config from "../config";
import autoBind from 'auto-bind';
import { Post } from '../models/post.model';
import { AuthService } from './auth.service';


@Injectable()
export class PostService {

	constructor(private http: HttpClient, private authService:AuthService) { }

	async getPosts():Promise<Array<Post>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/posts", {headers}).toPromise();

		let posts:Array<Post> = [];
		for (let i in results){
			let post = new Post(results[i]);
			posts.push(post);
		}
		posts.sort(function(a, b){
			return b.creationDate.getTime() - a.creationDate.getTime();
		});
		return posts;
	}

	async getPostsByTag(tagId:string):Promise<Array<Post>>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let results = await this.http.get(Config.host + "/posts/byTag/" + tagId, {headers}).toPromise();

		let posts:Array<Post> = [];
		for (let i in results){
			let post = new Post(results[i]);
			posts.push(post);
		}
		posts.sort(function(a, b){
			return b.creationDate.getTime() - a.creationDate.getTime();
		});
		return posts;
	}

	async getPost(postId:string):Promise<Post>{
		let headers = new HttpHeaders().set('content-type', 'application/json');
		let result = await this.http.get(Config.host + "/posts/" + postId, {headers}).toPromise();
		return new Post(result);
	}

	async createPost(post:Post):Promise<Post>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.post(Config.host + "/posts", post, {headers}).toPromise();
		return new Post(result);
	}

	async updatePost(post:Post):Promise<Post>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.put(Config.host + "/posts/" + post._id, post, {headers}).toPromise();
		return new Post(result);
	}

	async deletePost(postId:string):Promise<any>{
		let headers = new HttpHeaders().set('content-type', 'application/json').set("Authorization", localStorage.getItem("authKey"));
		let result = await this.http.delete(Config.host + "/posts/" + postId, {headers}).toPromise();
	}
}
