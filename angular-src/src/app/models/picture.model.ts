import autoBind from 'auto-bind';
import Config from '../config';

export class Picture{
    _id: string;
    description: string;
    changeDate: Date = new Date();

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.description = data.description;
            this.changeDate = new Date(data.changeDate);
        }
    }

    getImageSource():string{
        return Config.host + "/pictures/" + this._id + "/download" + "?v=" + this.changeDate.toJSON();
    }
}