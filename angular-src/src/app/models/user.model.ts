import autoBind from 'auto-bind';
import { Usertype } from './usertype.model';
import { Like } from './like.model';
import { Comment } from './comment.model';

export class User{
    _id: string;
    username: string;
    email: string;
    password: string;
    fullName: string;
    userType: Usertype;
    likes: Like[] = [];
    comments: Comment[] = [];

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.username = data.username;
            this.email = data.email;
            this.password = data.password;
            this.fullName = data.fullName;
            this.userType = new Usertype(data.userType);
            if (data.likes == null){
                this.likes = [];
            } else {
                this.likes = data.likes.map(function(like:any){ return new Like(like); });
            }
            if (data.comments == null){
                this.comments = [];
            } else {
                this.comments = data.comments.map(function(comment:any){ return new Comment(comment); });
            }
        }
    }
}