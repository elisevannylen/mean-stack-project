import autoBind from 'auto-bind';

export class Tag{
    _id: string;
    tagName: string;

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.tagName = data.tagName;
        }
    }
}