import autoBind from 'auto-bind';
import { Picture } from './picture.model';
import { Tag } from './tag.model';
import { Comment } from './comment.model';
import { Like } from './like.model';
import { User } from './user.model';
import { Posttext } from './posttext.model';

export class Post{
    _id: string;
    title: string;
    description: string;
    writeDate: string;
    creationDate: Date;
    creator: User = new User();
    likes: Like[] = [];
    comments: Comment[] = [];
    tags: Tag[] = [];
    pictures: Picture[] = [];
    texts: Posttext[] = [];

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.title = data.title;
            this.description = data.description;
            this.writeDate = data.writeDate;
            this.creationDate = new Date(data.creationDate);
            this.creator = new User(data.creator);
            if (data.tags == null){
                this.tags = [];
            } else {
                this.tags = data.tags.map(function(tag:any){ return new Tag(tag); });
            }
            if (data.likes == null){
                this.likes = [];
            } else {
                this.likes = data.likes.map(function(like:any){ return new Like(like); });
            }
            if (data.texts == null){
                this.texts = [];
            } else {
                this.texts = data.texts.map(function(text:any){ return new Posttext(text); });
            }
            if (data.comments == null){
                this.comments = [];
            } else {
                this.comments = data.comments.map(function(comment:any){ return new Comment(comment); });
            }
            if (data.pictures == null){
                this.pictures = [];
            } else {
                this.pictures = data.pictures.map(function(picture:any){ return new Picture(picture); });
            }
        }
    }
}

