import autoBind from 'auto-bind';

export class Usertype{
    _id: string;
    typeName: string;

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.typeName = data.typeName;
        }
    }

    getId(){
		return this._id;
	}
}