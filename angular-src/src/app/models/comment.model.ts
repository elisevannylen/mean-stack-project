import autoBind from 'auto-bind';
import { Post } from './post.model';
import { User } from './user.model';

export class Comment{
    _id: string;
    message: string;
    user: User;
    post: Post;

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.message = data.message;
            this.user = new User(data.user);
            this.post = new Post(data.post);
        }
    }
}