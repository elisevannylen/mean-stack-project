import autoBind from 'auto-bind';
import { Post } from './post.model';
import { User } from './user.model';

export class Like{
    _id: string;
    post: Post;
    user: User;

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.post = new Post(data.post);
            this.user = new User(data.user);
        }
    }
}