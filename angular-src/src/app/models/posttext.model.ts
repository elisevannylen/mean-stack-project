import autoBind from 'auto-bind';

export class Posttext{
    _id: string;
    text: string;

    constructor(data:any = null){
        if(data != null){
            this._id = data._id;
            this.text = data.text;
        }
    }
}