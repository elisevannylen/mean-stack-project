import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from './services/auth.service';
declare var $;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {
	title = 'app';

	constructor(private authService: AuthService){
		this.validateToken();
		this.refreshNavbar();
		this.authService.onLogin.subscribe(this.refreshNavbar);
		this.authService.onLogout.subscribe(this.refreshNavbar);
		this.authService.onUserFetched.subscribe(this.refreshNavbar);
		setTimeout(function(){
			$('[href="#navPanel"]').on("click", function(event){
				if ($('body').hasClass('navPanel-visible')){
					$('body').removeClass('navPanel-visible');
				} else {
					$('body').addClass('navPanel-visible');
				}
				event.preventDefault();
				event.stopPropagation();
			});
		}, 1000);
		
	}

	async validateToken(){
		try {
			await this.authService.validateToken();
		} catch (err) {
			this.authService.logout();
		}
	}

	refreshNavbar(){
		setTimeout(function(){
		let $body = $('body');
		let nav = $('[id="navPanel"]');
		nav.remove();

		// Navigation Panel.
		$(
			'<div id="navPanel">' +
				'<nav>' +
					$('#nav').navList() +
				'</nav>' +
			'</div>'
		)
			.appendTo($body)
			.panel({
				delay: 500,
				hideOnClick: true,
				hideOnSwipe: true,
				resetScroll: true,
				resetForms: true,
				side: 'left',
				target: $body,
				visibleClass: 'navPanel-visible'
			});
		}, 1000);
	}
}
