import { environment } from '../environments/environment';

let host = "https://exchange.vannylen.eu";
if (!environment.production){
    host = "http://localhost:3000";
}

export default class Config{
	static host:string = host;
	static basesite:string = host;
}
  
